from rest_framework import permissions


class IsAccessTask(permissions.BasePermission):

    def has_object_permission(self, request, view, obj):
        if view.action == 'retrieve':
            return obj.owner == request.user or request.user in obj.added_users.all()
        elif view.action in ['destroy', 'update', 'partial_update', 'set_user']:
            return obj.owner == request.user
        return True

