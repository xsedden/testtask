from django.db import models


class Task(models.Model):
    created_at = models.DateField('Дата создания', auto_now_add=True)
    end_date = models.DateTimeField('Дата наступления')
    title = models.CharField('Заголовок', max_length=255)
    description = models.CharField('Текст задачи', max_length=255)
    place = models.CharField('Место', max_length=100)
    owner = models.ForeignKey('auth.User', related_name='self_tasks', on_delete=models.CASCADE)
    finished = models.BooleanField('Завершенная', default=False)
    sended = models.BooleanField('Отправленная', default=False)
    added_users = models.ManyToManyField('auth.User',
                                         verbose_name='Добавленные пользователи',
                                         related_name='tasks')
