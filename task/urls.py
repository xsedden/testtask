from django.urls import path
from rest_framework import routers

from . import views, models


app_name = 'task'

router = routers.SimpleRouter()
router.register(r'tasks', views.TaskViewSet)

urlpatterns = []

urlpatterns += router.urls
