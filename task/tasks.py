import datetime
from django.conf import settings

from django.core.mail import send_mail

from testtask.celery import app
from task.models import Task as model_Task

from celery import Task


# class MyTaskClass(Task):

#     def run(self, *args, **kwargs):
#         print(1)
#         tasks = model_Task.objects.filter(end_date__gte=datetime.datetime.now(), sended=False, finished=False)
#         for task in tasks:
#             emails = list(task.added_users.values_list('email', flat=True)).append(task.owner.email)
#             send_mail(
#                 task.title,
#                 task.description,
#                 settings.EMAIL_HOST_USER,
#                 emails,
#                 fail_silently=False,
#             )
#             task.sended = True
#             task.save()

# @app.on_after_configure.connect
# def setup_periodic_tasks(sender, **kwargs):
#     sender.add_periodic_task(60.0, send_all_mails.s())

@app.task
def send_all_mails():
    tasks = model_Task.objects.filter(end_date__gte=datetime.datetime.now(), sended=False, finished=False)
    for task in tasks:
        emails = list(task.added_users.values_list('email', flat=True)).append(task.owner.email)
        send_mail(
            task.title,
            task.description,
            settings.EMAIL_HOST_USER,
            emails,
            fail_silently=False,
        )
        task.sended = True
        task.save()

