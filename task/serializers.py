from datetime import datetime

from rest_framework import serializers
from django.contrib.auth.models import User

from task.models import Task


class UsersSerializer(serializers.RelatedField):

    def to_representation(self, value):
        return value.username

    class Meta:
        model = User


class TaskSerializer(serializers.ModelSerializer):
    owner = serializers.ReadOnlyField(source='owner.username')
    added_users = UsersSerializer(read_only=True, many=True)

    def validate_end_date(self, value):
        if datetime.now() > value:
            raise serializers.ValidationError("Дата наступления должна быть больше текущей")
        return value

    class Meta:
        model = Task
        exclude = ('sended',)
        extra_kwargs = {'created_at': {'read_only': True}}



