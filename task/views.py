from django.contrib.auth.models import User
from django.shortcuts import get_object_or_404
from django.db.models import Q

from rest_framework import viewsets
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated
from rest_framework.decorators import action
from rest_framework import status

from task.permissions import IsAccessTask
from task.serializers import TaskSerializer
from task.models import Task


class TaskViewSet(viewsets.ModelViewSet):

    permission_classes = [IsAuthenticated, IsAccessTask]

    serializer_class = TaskSerializer
    queryset = Task.objects.all()

    def perform_create(self, serializer):
        serializer.save(owner=self.request.user)

    def get_queryset(self):
        user = self.request.user
        owner = self.request.query_params.get('owner', False)
        if owner == 'True':
            return Task.objects.filter(owner=user, finished=False)
        return Task.objects.filter(Q(Q(owner=user) | Q(added_users=user)) & Q(finished=False))

    @action(detail=True, methods=['put'], url_path='set_user/(?P<user_pk>[^/.]+)')
    def set_user(self, request, user_pk, pk=None):
        task = self.get_object()
        user = get_object_or_404(User, pk=user_pk)
        if user == self.request.user or user in task.added_users.all():
            return Response({'error': 'Исходные параметры не допустимы'}, status=status.HTTP_400_BAD_REQUEST)
        task.added_users.add(user)
        return Response(status=status.HTTP_200_OK)


