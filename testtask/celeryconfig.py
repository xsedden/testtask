from kombu import Queue, Exchange

broker_url = 'pyamqp://rabbitmq:5672'

task_default_queue = 'task'

task_queues = (
    Queue('task', Exchange('task'), routing_key='task'),
)

imports = ('task.tasks')

