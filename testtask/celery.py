import os
import sys
from celery import Celery
from django.conf import settings
from celery.schedules import crontab

from testtask import celeryconfig

sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'testtask.settings')

app = Celery('task')

app.config_from_object(celeryconfig)

app.conf.task_routes = {'task.tasks.*': {'queue': 'task', 'routing_key': 'task'}}

app.conf.beat_schedule = {
    'send_all_mails': {
        'task': 'task.tasks.send_all_mails',
        'schedule': crontab(minute='*/1')
    },
}
app.autodiscover_tasks(lambda: settings.INSTALLED_APPS)

if __name__ == '__main__':
    app.start()
