from django.urls import path
from rest_framework.authtoken import views as gen_views
from rest_framework import routers

from . import views, models


app_name = 'core'

router = routers.SimpleRouter()
router.register(r'users', views.UserViewSet)

urlpatterns = [
    path('login/', gen_views.obtain_auth_token),
]

urlpatterns += router.urls
