from django.contrib.auth.models import User
from rest_framework import status
from rest_framework import viewsets
from rest_framework.decorators import action
from rest_framework.response import Response
from core.serializers import UserSerializer
from rest_framework import mixins
from core.permissions import IsSelfUserOrCreateOnly


class UserViewSet(mixins.CreateModelMixin,
                  viewsets.GenericViewSet):

    queryset = User.objects.all()
    serializer_class = UserSerializer
    permission_classes = [IsSelfUserOrCreateOnly]

    @action(detail=True, methods=['get'])
    def profile(self, request, *args, **kwargs):
        instance = request.user
        serializer = self.get_serializer(instance)
        return Response(serializer.data)


