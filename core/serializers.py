from django.contrib.auth.models import User
from rest_framework import serializers


class UserSerializer(serializers.ModelSerializer):

    def create(self, v_d):
        user = User.objects.create(**v_d)
        user.set_password(v_d['password'])
        user.save()
        return v_d


    class Meta:
        model = User
        exclude = ('last_login', 'is_active', 'is_staff', 'date_joined' , 'is_superuser', 'groups', 'user_permissions')
        extra_kwargs = {'password': {'write_only': True}}

